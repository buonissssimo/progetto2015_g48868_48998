package it.unica.pr2.progetto2015.g48868_48998;
import com.dropbox.core.*;
import java.io.*;
import java.util.*;
import static it.unica.pr2.progetto2015.g48868_48998.HumanSizer.makeReadable;
import static it.unica.pr2.progetto2015.g48868_48998.PercentageCalculator.calculatePercentage;


/**
 *
 * @author Giovanni Laerte Frongia && Luca Pitzalis
 */


public class Custom implements it.unica.pr2.progetto2015.interfacce.SheetFunction{
    
        final String APP_KEY = "evmt55wiqj9s3wf";
        final String APP_SECRET = "g7z2b4k13rasomk";


        
    @Override
    public Object execute(Object... args){
        
        if (args[0].equals("reg")) 
            return firstTimeUser((String)args[1], (String)args[2]); 
        else
            return createTable(signedUser((String) args[0]));
        
    }
    
    private String[][] createTable(ArrayList<RigaTabella> arrayElementi){
        
        String[][] table = new String[arrayElementi.size()+1][4];
        int totalSize = 0;
        
        table[0][0]="Estensione";
        table[0][1]="N. Files";
        table[0][2]="Bytes";
        table[0][3]="% of total";
        
        for (RigaTabella i : arrayElementi)
            totalSize += i.getDimensione();
                
        for(int j = 1; j < arrayElementi.size() + 1; j++){
            table[j][0] = (arrayElementi.get(j-1).getEstensione());
            table[j][1] = (((Integer) arrayElementi.get(j-1).getNFiles()).toString());
            table[j][2] = makeReadable(arrayElementi.get(j-1).getDimensione());
            table[j][3] = String.format("%.3f", calculatePercentage(totalSize, arrayElementi.get(j-1).getDimensione() ) ) + "%" ;
        }
        
        return table;
    }
    
    private String firstTimeUser(String code, String nome){
                   
        DbxAppInfo appInfo = new DbxAppInfo(APP_KEY, APP_SECRET);

        DbxRequestConfig config = new DbxRequestConfig(
            "JavaTutorial/1.0", Locale.getDefault().toString());
        DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(config, appInfo);


        String authURL = webAuth.start();

        try{

            DbxAuthFinish authFinish = webAuth.finish(code);
            String accessToken = authFinish.accessToken;
            DbxClient client = new DbxClient(config, accessToken); 

            File dataBase = new File(System.getProperty("user.dir"), "dataBase.txt");

            if(!dataBase.exists()) {
                dataBase.createNewFile();
            } 

            PrintWriter printWriter = new PrintWriter(dataBase);

            printWriter.println(nome + "=" + accessToken);

            printWriter.close(); 


        }
        catch(Exception e){ 
            System.out.println(e.getMessage());
            return "Errore";
        }


        return "Registrazione avvenuta con successo. Ora puoi chiamare la funzione usando il tuo nome utente";
        
    }
    
    private ArrayList<RigaTabella> signedUser(String nome){
    	
    	File database = new File(System.getProperty("user.dir"), "dataBase.txt");
        DbxAppInfo appInfo = new DbxAppInfo(APP_KEY, APP_SECRET);
        DbxClient client;

        try{
            Scanner s = new Scanner(database).useDelimiter("=");
            String accessToken = "";
            
            while(s.hasNextLine()){
                    String name = s.next();
                    if(name.equals(nome)){
                        accessToken = s.next();
                    }
            }
            
            accessToken = accessToken.trim();
            DbxRequestConfig config = new DbxRequestConfig("JavaTutorial/1.0", Locale.getDefault().toString());
            DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(config, appInfo);

            client = new DbxClient(config, accessToken);

            return ListFiles(client);
        }
        catch(Exception e){
            System.out.println(e.getMessage() + e.toString() + "LINEA 107");
            return null;
        }
        
    }
    
    private ArrayList<RigaTabella> ListFiles(DbxClient client) throws DbxException{
    	
    	ArrayList<RigaTabella> tabella = new ArrayList<RigaTabella>();
    	String estensione;
    	DbxEntry.WithChildren listing = client.getMetadataWithChildren("/");
        int lastDot = 0;
        System.out.println(listing.children.size());

        for (DbxEntry child : listing.children) {
            
            if (!child.toString().startsWith("Folder")){
               
                if (child.name.contains(".")){
                    lastDot = child.name.lastIndexOf('.'); 
                     estensione = child.name.substring(lastDot+1);
                }
                else
                    estensione = "NoExtension";

                int dimensione = calculateBytes(child.toString());
                
                boolean endOfTable = true;
                
                for(RigaTabella current : tabella){
                    if(current.getEstensione().equals(estensione)){
                        current.updateFile(calculateBytes(child.toString()));
                        endOfTable = false;
                        break;
                    }
                endOfTable = true;
                }
                if(endOfTable)
                    tabella.add(new RigaTabella(estensione, dimensione));

                }

        }

        for (RigaTabella i : tabella)
            System.out.println(i.getEstensione()+ " " + i.getDimensione() + " " + i.getNFiles() );
		
        return tabella;
    }
    
    private int calculateBytes(String s){
    
        int lastNumBytes = s.indexOf("numBytes=");
        int nextComma = s.indexOf(",", lastNumBytes);
        int numBytes = Integer.parseInt(s.substring(lastNumBytes+9, nextComma));
        
        return numBytes;
    }
    
    @Override
    public final String getName(){
        return "DROPBOX.STATISTICS";
    }
    @Override
    public final String getCategory(){
        return "DROPBOX";
    }
    @Override
    public final String getHelp(){
        return "Consegna informazioni riguardanti un account di Dropbox: Estensioni, Numero di file per estensione, Spazio occupato per estensione.";
    }
}