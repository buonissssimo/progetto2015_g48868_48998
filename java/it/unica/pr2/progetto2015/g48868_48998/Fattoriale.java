/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g48868_48998;

/**
 *
 * @author bardOZ
 */
public class Fattoriale {
   
    public static int fact(int n){
        int result=1;
        
        for (int i = 1; i<n+1; i++){
            result*=i;
        }
        
        return result;
        
    }
}
