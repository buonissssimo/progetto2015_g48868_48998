package it.unica.pr2.progetto2015.g48868_48998;

public class Complessa implements it.unica.pr2.progetto2015.interfacce.SheetFunction {
    /** 
    Argomenti in input ed output possono essere solo: String, Integer, Long, Double, Character, Boolean e array di questi tipi.
    Ad esempio a runtime si puo' avere, come elementi di args, un Integer ed un Long[], e restituire un Double[];
    */
    @Override
    public Object execute(Object... args) {
		                
        //double result; 
        
        if (args instanceof Double[][]){
            return calcSingleMatrix((Double[][]) args);
        }

        if (args instanceof Object[]){
            return calcMultipleMatrices(args);
        }
        
        return ("#VALUE");
    }

    
    private Double calcSingleMatrix(Double[][] matrix){
    
        double sum = 0;
        
        for (Double[] d : matrix)
            for (Double i : d)
                sum+=i;
        
        return sum;
    }

    private Double calcMultipleMatrices(Object[] args){
        Integer nRows = ((Double[][])args[0]).length;
        Integer nCols = ((Double[])((Double[][])args[0])[0]).length;
        
        Double[][] suppMatr = (Double[][])args[0];
        
        for (int i = 1; i < args.length; i++)
            for (int row = 0; row < nRows; row++)
                for (int col = 0; col < nCols; col++)
                    suppMatr[row][col] *= ((Double[][])args[i])[row][col];
    
        double sum = 0;

        for (Double[] d : suppMatr)
            for (Double i : d)
                sum+=i;
        
        return sum;
    }
    
    /**
    Restituisce la categoria LibreOffice;
    Vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad esempio, si puo' restituire "Data&Orario" oppure "Foglio elettronico"
    */
    public final String getCategory() {
        return "Matrice";
    }

    // Informazioni di aiuto 
    public final String getHelp() {
        return "Moltiplica gli elementi corrispondenti nelle matrici indicate e restituisce la somma dei prodotti.";
    } 

    /**
    Nome della funzione.
    vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad es. "VAL.DISPARI" 
    */
    public final String getName() {
        return "MATR.SOMMA.PRODOTTO";
    }

}
