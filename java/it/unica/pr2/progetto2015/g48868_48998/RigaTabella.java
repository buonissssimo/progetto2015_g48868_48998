package it.unica.pr2.progetto2015.g48868_48998;




public class RigaTabella{
	
	private String estensione;
	private int nFiles;
	private int dimensione;
	
	public RigaTabella(String estensione, int dimensione){
		this.estensione = estensione;
		this.dimensione=dimensione;
		nFiles++;
	}
        
	public String getEstensione(){
		return estensione;
	}
	
	public int getNFiles(){
		return nFiles;
	}
	
	public int getDimensione(){
		return dimensione;
	}
	
	public void updateFile(int dimensione){
		nFiles++;
		this.dimensione+=dimensione;
	}
	
	
	
	
}