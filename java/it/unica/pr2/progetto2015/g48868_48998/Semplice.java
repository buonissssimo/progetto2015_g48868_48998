/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g48868_48998;
import static it.unica.pr2.progetto2015.g48868_48998.Fattoriale.fact;


/**
 *
 * @author bardOZ
 */
public class Semplice implements it.unica.pr2.progetto2015.interfacce.SheetFunction{
        
    @Override
    public Object execute(Object... args){
        //Formula da implementare: (Numero 1+Numero 2-1)! / (Numero 1!(Numero 1-1)!)
        int setCardinality = (Integer) args[0];
        int nToChoose = (Integer) args[1]; 
                
        return ( fact((setCardinality+nToChoose-1)) / ( (fact(nToChoose) * fact(setCardinality-1)) ));
    }
    @Override
    public final String getName(){
        return "COMBINAZIONE.VALORI";
    }
    @Override
    public final String getCategory(){
        return "Matematica";
    }
    @Override
    public final String getHelp(){
        return "Restituisce il numero di permutazioni per un sottoinsieme di elementi che includono le ripetizioni.";
    }
    
    
}
