PROGETTO PR2 2015

FUNZIONE SEMPLICE:
	La funzione Semplice prende in ingresso 2 celle contenenti numeri interi e calcola la combinazione con ripetizione.
	COME USARE:
		1) Creare un oggetto di tipo Semplice in una cella qualsiasi del foglio di calcolo tramite la seguente funzione OBBA: =obMake("nome_oggetto", "pacchetto_classe_Semplice")
		2) Chiamare il metodo execute dell'oggetto appena creato:  =obCall("nome"; Cella Oggetto; "execute"; obMake("nome_oggetto"; "Integer"; CellaPrimoNumero); obMake("nome_oggetto"; "Integer"; CellaSecondoNumero))
		3) Far restituire il risultato: =obGet(Cella in cui si è effettuata la obCall)

FUNZIONE COMPLESSA:
	La funzione Complessa prende in ingresso un numero n > 0 di matrici di uguale dimensione e calcola la somma dei prodotti tra gli elementi delle matrici.
	COME USARE:
		1) Creare un oggetto di tipo Complessa in una cella qualsiasi del foglio di calcolo tramite la seguente funzione OBBA: =obMake("nome_oggetto", "pacchetto_classe_Complessa")
		2) Chiamare il metodo execute dell'oggetto appena creato:  =obCall("nome"; Cella Oggetto; "execute"; obMake("nome_oggetto"; "Double[][]"; PrimaMatrice); ......; obMake("nome_oggetto"; "Double[][]"; NsimaMatrice))
		3) Far restituire il risultato: =obGet(Cella in cui si è effettuata la obCall)
		
FUNZIONE CUSTOM
	La funzione Custom restituisce una tabella in cui vengono elencate tutte le estensini dei file presenti nella root dropbox dell'utente con relativa quantità di file, dimensione e percentuale di spazio occupata.
	Per poter usare tale funzione bisogna prima autorizzare l'applicazione tramite il seguente link:
		https://www.dropbox.com/1/oauth2/authorize?locale=it_IT&client_id=evmt55wiqj9s3wf&response_type=code
	Una volta autorizzata bisognerà copiare il token che viene mostrato nella finestra.
	
	Il test di questa classe non funzionerà perchè per l'accesso sono stati utilizzati i dati di dei membri del gruppo. Per poterlo utilizzare è necessario seguire la procedura spiegata sotto dopo che si è autorizzata l'applicazione su dropbox.  
	
	COME USARE:
		1) Creare un oggetto di tipo Custom in una cella qualsiasi del foglio di calcolo tramite la seguente funzione OBBA: =obMake("nome_oggetto", "pacchetto_classe_Custom")
		2) Chiamare il metodo execute dell'oggetto appena creato: 
				Utente non registrato: =obCall("nome"; Cella Oggetto; "execute"; obMake("nome_oggetto"; "String"; "reg");obMake("nome_oggetto"; "String"; "token_copiato");obMake("nome_oggetto"; "String"; "nome_utente"))
				Se l'utente è già esistente o è appena stato creato:
					Utente registrato: =obCall("nome"; Cella Oggetto; "execute"; obMake("nome_oggetto"; "String"; "nome_utente"))
		3) Far restituire il risultato: =obGet(Cella in cui si è effettuata la obCall)